# Botler

Create new "room services" with a classic form in popup dialog. Just select what events and terms you want Botler to listen to and then enter what you want Botler to say when that happens.

For you advanced users, you can use regular expression for smarter phrase detection and even use captured terms in your replies.

By default, Botler's replies will be rendered just like your normal messages, which means that your @mentions and emoticons will appear as expected. You can opt-in to use HTML rendering instead if you're a fancy hypertext wizard.

## Development

To get start, clone this repository and install it's dependencies

```
npm install
```

### Database

Now you'll need a local postgres database to store Botlers state.

Create a database with the name `room_service` (see `/config/config.js`).

You'll also need to change the username in `/config/config.js` to one you have locally.


Now set up the database and migrations by running

```
npm run prestart
```

You will need to redo this anytime there is a database change.

### ngrok

You'll also need a local ngrok server running to reroute traffic from the internet to your local server.

Do this by running

```
ngrok http 3000
```

It should give you two ngrok urls. Copy the `https` one (e.g. `https://a194edea.ngrok.io`) into `/config.json` in the `localBaseUrl` field.

You can now build Botler itself using

```
npm run build
```

And run the server itself using

```
npm start
```

You should be able to confirm that your server is working by visiting `localhost:3000/atlassian-connect.json` or the ngrok address from `/config.json`.

### Installing

You'll now be able to install your local addon into an actual Hipchat room by going to the room and clicking `Integrations` -> `Install new integrations`, then clicking the `Install an integration from a descriptor URL` link at the bottom of the page.

When the modal opens, enter your ngrok url (e.g https://a194edea.ngrok.io/atlassian-connect.json) and follow the prompts.