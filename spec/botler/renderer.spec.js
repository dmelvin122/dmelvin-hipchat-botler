import renderer from '../../src/botler/renderer';

describe('renderer', () => {

    let mockRes;

    beforeEach(() => {
        mockRes = {
            render: jasmine.createSpy('render')
        };
    })

    it('should call res.render with index view and payload', () => {
        const payload = {
            some: 'value'
        };

        renderer({}, mockRes, payload);
        expect(mockRes.render).toHaveBeenCalledWith('index', payload);
    });

    it('should apply identity to the payload if available', () => {
        const mockReq = {
            identity: {
                some: 'key'
            }
        }
        
        const payload = {
            some: 'value'
        };

        renderer(mockReq, mockRes, payload);
        expect(mockRes.render).toHaveBeenCalledWith('index', Object.assign({}, payload, mockReq));
    });

    it('should apply hipChatTheme to the payload if available', () => {
        const mockReq = {
            query: {
                theme: 'dark'
            }
        }
        
        const payload = {
            some: 'value'
        };

        const expectedPayload = {
            some: 'value',
            hipChatTheme: 'dark'
        };

        renderer(mockReq, mockRes, payload);
        expect(mockRes.render).toHaveBeenCalledWith('index', expectedPayload);
    });

    it('should not apply hipChatTheme if it has already been set', () => {
        const mockReq = {
            query: {
                theme: 'dark'
            }
        }
        
        const payload = {
            some: 'value',
            hipChatTheme: 'dark'
        };

        renderer(mockReq, mockRes, payload);
        expect(mockRes.render).toHaveBeenCalledWith('index', payload);
    });
});