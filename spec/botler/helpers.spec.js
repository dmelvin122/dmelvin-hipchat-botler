import botlerHelpers from '../../src/botler/helpers';

const BotlerHelpers = botlerHelpers({}); 

describe('Botler Helpers', function() {
    describe('checkForMatchTerm', function() {
        it('should return the correct integer value', () => {
            expect(BotlerHelpers.checkForMatchTerm('{{$1}}')).toBe(1);
            expect(BotlerHelpers.checkForMatchTerm('{{$2}}')).toBe(2);
            expect(BotlerHelpers.checkForMatchTerm('{{$3}}')).toBe(3);
        });

        it('should return the null when undefined', () => {
            expect(BotlerHelpers.checkForMatchTerm()).toBe(null);
        });

        it('should return the undefined when no matched terms', () => {
            expect(BotlerHelpers.checkForMatchTerm('asdf')).not.toBeDefined();
        });
    });

    describe('checkForMatchTerm', function() {
        it('should return true if event has changed', () => {
            const service = {
                event: 'room_message',
                pattern: 'hello'
            };
            const oldEvent = 'room_enter';
            const oldPattern = '';

            expect(BotlerHelpers.checkIfWebhookChanged(service, oldEvent, oldPattern)).toBe(true);
        });

        it('should return true if webhook has changed', () => {
            const service = {
                event: 'room_message',
                pattern: '^/hello'
            };
            const oldEvent = 'room_message';
            const oldPattern = 'hello';

            expect(BotlerHelpers.checkIfWebhookChanged(service, oldEvent, oldPattern)).toBe(true);
        });

        it('should return false not changed', () => {
            const service = {
                event: 'room_message',
                pattern: '^/hello'
            };
            const oldEvent = 'room_message';
            const oldPattern = '^/hello';

            expect(BotlerHelpers.checkIfWebhookChanged(service, oldEvent, oldPattern)).toBe(false);
        });

        it('should return true when service event changes', () => {
            const service = {
                event: 'room_message',
                pattern: '^/hello'
            };
            const oldEvent = 'room_enter';
            const oldPattern = '';

            expect(BotlerHelpers.checkIfWebhookChanged(service, oldEvent, oldPattern)).toBe(true);
        });
    });

    describe('validateService', () => {
        it('should return false if name is missing', () => {
            const testService = {
                event: 'room_message',
                pattern: 'test'
            };

            expect(BotlerHelpers.validateService(testService)).toBe(false);
        });

        it('should return false if pattern is missing when event is room_message', () => {
            const testService = {
                name: 'test service',
                event: 'room_message'
            };

            expect(BotlerHelpers.validateService(testService)).toBe(false);
        });

        it('should return true for valid room_message service', () => {
            const testService = {
                name: 'test service',
                event: 'room_message',
                pattern: '^/hello'
            };

            expect(BotlerHelpers.validateService(testService)).toBe(true);
        });

        it('should return true for valid room_enter service', () => {
            const testService = {
                name: 'test service',
                event: 'room_enter'
            };

            expect(BotlerHelpers.validateService(testService)).toBe(true);
        });
    });
});
