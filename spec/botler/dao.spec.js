import dao from '../../src/botler/dao';

describe('data access object', () => {

    let addon;
    let BotlerDAO;

    beforeEach(() => {
        // setup mocks and spies for database query calls
        addon = {
            db: {
                AddonSetting: {
                    findOne: jasmine.createSpy('AddonSetting.findOne')
                },
                RoomService: {
                    findOne: jasmine.createSpy('RoomService.findOne'),
                    findAll: jasmine.createSpy('RoomService.findAll')
                }
            }
        }
        BotlerDAO = dao(addon);
    });

    describe('getAddonSettingByClientKey', () => {
        it('should call AddonSetting findOne with the correct query', () => {
            const clientKey = '123adbasdf';
            BotlerDAO.getAddonSettingByClientKey(clientKey);
            expect(addon.db.AddonSetting.findOne).toHaveBeenCalledWith({
                where: {
                    clientKey: clientKey,
                    key: 'clientInfo'
                }
            });
        });
    });

    describe('getServicesByClientKey', () => {
        it('should call RoomService findAll with the correct query', () => {
            const clientKey = '123adbasdf';
            BotlerDAO.getServicesByClientKey(clientKey);
            expect(addon.db.RoomService.findAll).toHaveBeenCalled();
            expect(addon.db.RoomService.findAll.calls.mostRecent().args[0].where.clientKey).toEqual(clientKey);
        });
    });

    describe('getServiceById', () => {
        it('should call RoomService findOne with the correct query', () => {
            const serviceId = 241;
            BotlerDAO.getServiceById(serviceId);
            expect(addon.db.RoomService.findOne).toHaveBeenCalled();
            expect(addon.db.RoomService.findOne.calls.mostRecent().args[0].where.id).toEqual(serviceId);
        });
    });
});