export default (props={}) => ({
    control: {
        backgroundColor: "#fff",
        fontFamily: 'inherit',
        fontSize: '14px',
        fontWeight: "normal",
    },

    input: {
        margin: '0px',
        width: '406px',
        height: '95px',
        padding: '4px 5px',
        border: '1px solid #ccc',
        borderRadius: '3.01px',
        boxSizing: 'border-box',
        fontSize: 'inherit'
    },

    "&singleLine": {
        control: {
            display: "inline-block",

            width: 130,
        },

        highlighter: {
            padding: 1,
            border: "2px inset transparent",
        },

        input: {
            padding: 1,

            border: "2px inset",
        },
    },

    "&multiLine": {
        control: {
            fontFamily: "monospace",
            width: '100%',
            border: "1px solid silver",
        },

        highlighter: {
            padding: 9,
        },

        input: {
            width: '100%',
            padding: 9,
            minHeight: 63,
            outline: 0,
            border: 0,
        }
    },

    suggestions: {
        border: "1px solid rgba(0,0,0,0.15)",
        fontSize: 10,

        item: {
            padding: "5px 15px",
            borderBottom: "1px solid rgba(0,0,0,0.15)",

            "&focused": {
                backgroundColor: "#cee4e5",
            }
        }
    }
});