export const CREATE_SERVICE = 'create';
export const EDIT_SERVICE = 'edit';
export const DIALOG_KEY = 'botler.dialog';
export const DIALOG_PRIMARY_ACTION_KEY = 'botler.dialog.action';
export const DIALOG_DELETE_ACTION_KEY = 'botler.dialog.delete';
export const DIALOG_CLOSE_ACTION_KEY = 'botler.dialog.close';
export const DIALOG_CREATE_FLOW_NEXT_KEY = 'botler.dialog.createflow.next';
export const DIALOG_CREATE_FLOW_BACK_KEY = 'botler.dialog.createflow.back';
export const DIALOG_CREATE_SAVE_KEY = 'botler.dialog.save';

export const CREATE_DIALOG_OPTIONS_ON = { 
    options: {
        primaryAction: {
            name: 'Save',
            key: DIALOG_PRIMARY_ACTION_KEY,
            enabled: true
        }
    }
};

export const CREATE_DIALOG_OPTIONS_OFF = { 
    options: {
        primaryAction: {
            name: 'Save',
            key: DIALOG_PRIMARY_ACTION_KEY,
            enabled: false
        }
    }
};

export const DIALOG_INIT_FLOW = {
    options: {
        primaryAction: null,
        secondaryActions: null,
        size: 'large'
    }
};

export const CREATE_FLOW_BUTTONS = {
    options: {
        primaryAction: {
            name: 'Next',
            key: DIALOG_CREATE_FLOW_NEXT_KEY
        },
        secondaryActions: [
            {
                name: 'Back',
                key: DIALOG_CREATE_FLOW_BACK_KEY
            },
            {
                name: 'Cancel'
            }
        ]
    }
}

export const IMPORT_BUTTONS = {
    options: {
        secondaryActions: [
            {
                name: 'Back',
                key: DIALOG_CREATE_FLOW_BACK_KEY
            },
            {
                name: 'Cancel'
            }
        ]
    }
}


export const EDIT_DIALOG_OPTIONS_ON = { 
    options: {
        primaryAction: {
            name: 'Save',
            key: DIALOG_PRIMARY_ACTION_KEY,
            enabled: true
        },
        secondaryActions: [
            {
                name: 'Delete',
                key: DIALOG_DELETE_ACTION_KEY,
                enabled: true
            },
            {
                name: 'Close',
                key: DIALOG_CLOSE_ACTION_KEY
            }
        ],
        size: 'large'
    }
};

export const EDIT_DIALOG_OPTIONS_OFF = { 
    options: {
        primaryAction: {
            name: 'Save',
            key: DIALOG_PRIMARY_ACTION_KEY,
            enabled: false
        },
        secondaryActions: [
            {
                name: 'Delete',
                key: DIALOG_DELETE_ACTION_KEY,
                enabled: false
            },
            {
                name: 'Close',
                key: DIALOG_CLOSE_ACTION_KEY
            }
        ],
        size: 'large'
    }
};
