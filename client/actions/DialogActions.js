import { openHipChatDialog, hipchatBroadcastEvent, showAUIFlag, updateHipChatDialog, closeHipChatDialog,
     EVENT_SIDEBAR_UPDATE } from './HipChatActions';
import { saveService, deleteService } from '../helpers/botler-requests';
import * as DIALOG_OPTS from './DialogOptions';

export function openCreateDialog() {
    openHipChatDialog(DIALOG_OPTS.DIALOG_KEY, DIALOG_OPTS.DIALOG_INIT_FLOW, {
        type: DIALOG_OPTS.CREATE_SERVICE
    });
}

export function openEditDialog(id) {
    openHipChatDialog(DIALOG_OPTS.DIALOG_KEY, DIALOG_OPTS.EDIT_DIALOG_OPTIONS_OFF, {
        type: DIALOG_OPTS.EDIT_SERVICE,
        serviceId: id
    });
}

export function enableDialogButtons(isCreateDialog) {
    if (isCreateDialog) {
        updateHipChatDialog(DIALOG_OPTS.CREATE_DIALOG_OPTIONS_ON);
    } else {
        updateHipChatDialog(DIALOG_OPTS.EDIT_DIALOG_OPTIONS_ON);
    }
}

export function disableDialogButtons(isCreateDialog) {
    if (isCreateDialog) {
        updateHipChatDialog(DIALOG_OPTS.CREATE_DIALOG_OPTIONS_OFF);
    } else {
        updateHipChatDialog(DIALOG_OPTS.EDIT_DIALOG_OPTIONS_OFF);
    }
}

export function removeButtons() {
    updateHipChatDialog(DIALOG_OPTS.DIALOG_INIT_FLOW);
}

function showErrorFlag(title, message) {    
    showAUIFlag('error', title, message)
}

export function saveBotlerService(service) {
    disableDialogButtons(!service.id)
    saveService(service, (err, res) => {
        if (!err) {
            hipchatBroadcastEvent(EVENT_SIDEBAR_UPDATE, res.body);
            closeHipChatDialog();
        } else {
            showErrorFlag('Failed to save service.', 'Please check your fields and try again.');
            enableDialogButtons(!service.id)
        }
    });
}

export function deleteBotlerService(service) {
    disableDialogButtons(false);
    deleteService(service.id, (err, res) => {
        if (!err) {
            hipchatBroadcastEvent(EVENT_SIDEBAR_UPDATE, res.body);
            closeHipChatDialog();
        } else {
            showErrorFlag('Failed to delete service.', 'Please try again later.');
            enableDialogButtons(false);
        }
    });
}