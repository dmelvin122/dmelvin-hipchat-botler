import * as HipChatActions from '../HipChatActions';

describe('HipChatActions', () => {

    beforeEach(() => {
        let HipChat = {
            dialog: {}
        };

        let AJS = {};
        window.HipChat = HipChat;
        window.AJS = AJS;
    });

    describe('openHipChatDialog', () => {
        it('should call HipChat.dialog.open', () => {
            HipChat.dialog.open = jasmine.createSpy('open');

            const key = 'botler.dialog';
            const options = {
                title: 'test'
            };
            const parameters = {
                id: 123
            };

            HipChatActions.openHipChatDialog(key, options, parameters);
            expect(HipChat.dialog.open).toHaveBeenCalledWith({ key, options, parameters });
        });
    });

    describe('updateHipChatDialog', () => {
        it('should call HipChat.dialog.update', () => {
            HipChat.dialog.update = jasmine.createSpy('update');

            const options = {
                title: 'test'
            };

            HipChatActions.updateHipChatDialog(options);
            expect(HipChat.dialog.update).toHaveBeenCalledWith(options);
        });
    });

    describe('closeHipChatDialog', () => {
        it('should call HipChat.dialog.close', () => {
            HipChat.dialog.close = jasmine.createSpy('close');
            HipChatActions.closeHipChatDialog();
            expect(HipChat.dialog.close).toHaveBeenCalled();
        });
    });

    describe('hipchatBroadcastEvent', () => {
        it('should call HipChat.broadcast', () => {
            HipChat.broadcast = jasmine.createSpy('broadcast');

            const event = 'SOME.EVENT.STRING';
            const data = {
                services: [
                    1, 2, 4
                ]
            };

            HipChatActions.hipchatBroadcastEvent(event, data);
            expect(HipChat.broadcast).toHaveBeenCalledWith(event, data);
        });
    });

    describe('showAUIFlag', () => {
        it('should call AJS.flag with the correct parameters', () => {
            AJS.flag = jasmine.createSpy('flag');
            const type = 'error';
            const title = 'this is a flag';
            const body = 'some test body';
            HipChatActions.showAUIFlag(type, title, body);
            expect(AJS.flag).toHaveBeenCalledWith({
                type: type,
                title: title,
                body: body,
                close: 'auto'
            });
        });
    });
});