import * as HipChatActions from '../HipChatActions';
import * as DialogActions from '../DialogActions';
import * as DIALOG_OPTS from '../DialogOptions';
import * as BotlerRequests from '../../helpers/botler-requests';

describe('DialogActions', () => {
    describe('openCreateDialog', () => {
        it('should call openHipChatDialog', () => {
            spyOn(HipChatActions, 'openHipChatDialog');
            DialogActions.openCreateDialog();
            expect(HipChatActions.openHipChatDialog).toHaveBeenCalled();
        });
    });

    describe('openEditDialog', () => {
        it('should call openHipChatDialog with the correct id', () => {
            spyOn(HipChatActions, 'openHipChatDialog');
            const id = 123;
            DialogActions.openEditDialog(id);
            expect(HipChatActions.openHipChatDialog).toHaveBeenCalled();
            expect(HipChatActions.openHipChatDialog.calls.mostRecent().args[2].serviceId).toEqual(id);
        });
    });

    describe('enableDialogButtons', () => {
        it('should call updateHipChatDialog with create options if true', () => {
            spyOn(HipChatActions, 'updateHipChatDialog');
            DialogActions.enableDialogButtons(true);
            expect(HipChatActions.updateHipChatDialog)
                .toHaveBeenCalledWith(DIALOG_OPTS.CREATE_DIALOG_OPTIONS_ON);
        });

        it('should call updateHipChatDialog with edit options if false', () => {
            spyOn(HipChatActions, 'updateHipChatDialog');
            DialogActions.enableDialogButtons(false);
            expect(HipChatActions.updateHipChatDialog)
                .toHaveBeenCalledWith(DIALOG_OPTS.EDIT_DIALOG_OPTIONS_ON);
        });
    });

    describe('disableDialogButtons', () => {
        it('should call updateHipChatDialog with create options if true', () => {
            spyOn(HipChatActions, 'updateHipChatDialog');
            DialogActions.disableDialogButtons(true);
            expect(HipChatActions.updateHipChatDialog)
                .toHaveBeenCalledWith(DIALOG_OPTS.CREATE_DIALOG_OPTIONS_OFF);
        });

        it('should call updateHipChatDialog with edit options if false', () => {
            spyOn(HipChatActions, 'updateHipChatDialog');
            DialogActions.disableDialogButtons(false);
            expect(HipChatActions.updateHipChatDialog)
                .toHaveBeenCalledWith(DIALOG_OPTS.EDIT_DIALOG_OPTIONS_OFF);
        });
    });

    describe('saveBotlerService', () => {
        const mockService = {
            name: 'test name',
            pattern: '^/hello',
            event: 'room_message',
            reply: 'Hello world!'
        };

        beforeEach(() => {
            spyOn(HipChatActions, 'openHipChatDialog');
            spyOn(HipChatActions, 'updateHipChatDialog');
            spyOn(HipChatActions, 'hipchatBroadcastEvent');
            spyOn(HipChatActions, 'closeHipChatDialog');
            spyOn(HipChatActions, 'showAUIFlag');
            spyOn(DialogActions, 'disableDialogButtons');
            spyOn(DialogActions, 'enableDialogButtons');
        });

        it('should call disableDialogButtons', () => {
            spyOn(BotlerRequests, 'saveService').and.callFake((service, callback) => {});
            DialogActions.saveBotlerService(mockService);
            expect(HipChatActions.updateHipChatDialog).toHaveBeenCalled();
        });

        it('should call hipChatBroadcastEvent and closeHipChatDialog if request is successful', (done) => {
            spyOn(BotlerRequests, 'saveService').and.callFake((service, callback) => {
                expect(service).toEqual(mockService);
                const mockRes = {body: 'test'};
                callback(false, mockRes);
                expect(HipChatActions.hipchatBroadcastEvent)
                    .toHaveBeenCalledWith(HipChatActions.EVENT_SIDEBAR_UPDATE, mockRes.body);
                expect(HipChatActions.closeHipChatDialog).toHaveBeenCalled();
                done();
            });
            DialogActions.saveBotlerService(mockService);
        });

        it('should call enableDialogButtons on error', (done) => {
            spyOn(BotlerRequests, 'saveService').and.callFake((service, callback) => {
                expect(service).toEqual(mockService);
                callback(true, {});
                expect(HipChatActions.updateHipChatDialog).toHaveBeenCalled();
                done();
            });
            DialogActions.saveBotlerService(mockService);
        });

        it('should call showAUIFlag on error', (done) => {
            spyOn(BotlerRequests, 'saveService').and.callFake((service, callback) => {
                expect(service).toEqual(mockService);
                callback(true, {});
                expect(HipChatActions.showAUIFlag).toHaveBeenCalled();
                done();
            });
            DialogActions.saveBotlerService(mockService);
        });
    });

    describe('deleteBotlerService', () => {

        beforeEach(() => {
            spyOn(HipChatActions, 'openHipChatDialog');
            spyOn(HipChatActions, 'updateHipChatDialog');
            spyOn(HipChatActions, 'hipchatBroadcastEvent');
            spyOn(HipChatActions, 'closeHipChatDialog');
            spyOn(HipChatActions, 'showAUIFlag');
            spyOn(DialogActions, 'disableDialogButtons');
            spyOn(DialogActions, 'enableDialogButtons');
        });

        it('should call disableDialogButtons', () => {
            spyOn(BotlerRequests, 'deleteService').and.callFake((service, callback) => {});
            DialogActions.deleteBotlerService(1);
            expect(HipChatActions.updateHipChatDialog).toHaveBeenCalled();
        });

        it('should call hipChatBroadcastEvent and closeHipChatDialog if request successful', (done) => {
            const id = 123;
            spyOn(BotlerRequests, 'deleteService').and.callFake((serviceId, callback) => {
                expect(serviceId).toEqual(id);
                const mockRes = {body: 'test'};
                callback(false, mockRes);
                expect(HipChatActions.hipchatBroadcastEvent)
                    .toHaveBeenCalledWith(HipChatActions.EVENT_SIDEBAR_UPDATE, mockRes.body);
                expect(HipChatActions.closeHipChatDialog).toHaveBeenCalled();
                done();
            });
            DialogActions.deleteBotlerService({id: id});
        });

        it('should call enableDialogButtons on error', (done) => {
            spyOn(BotlerRequests, 'deleteService').and.callFake((service, callback) => {
                callback(true, {});
                expect(HipChatActions.updateHipChatDialog).toHaveBeenCalled();
                done();
            });
            DialogActions.deleteBotlerService({id: 123});
        });

        it('should call showAUIFlag on error', (done) => {
            spyOn(BotlerRequests, 'deleteService').and.callFake((service, callback) => {
                callback(true, {});
                expect(HipChatActions.showAUIFlag).toHaveBeenCalled();
                done();
            });
            DialogActions.deleteBotlerService({id: 123});
        });
    });
});
