// LOTS OF RED - ☹️

export const EVENT_SIDEBAR_UPDATE = 'botler.event.sidebar.update';

export function checkAllJSPresent() {
    if (HipChat) {
        console.log('HipChat all.js is present.');
    } else {
        console.log('HipChat all.js is not present.');
    }
}

export function openHipChatDialog(key, options, parameters) {
    HipChat.dialog.open({key, options, parameters});
}

export function updateHipChatDialog(options) {
    HipChat.dialog.update(options);
}

export function closeHipChatDialog() {
    HipChat.dialog.close();
}

export function updatePrimaryAction(options) {
    HipChat.dialog.updatePrimaryAction(options);
}

export function hipchatBroadcastEvent(event, data) {
    HipChat.broadcast(event, data);
}

export function getCurrentUser(callback) {
    HipChat.user.getCurrentUser(callback);
}

export function getRoomDetails(callback) {
    HipChat.room.getRoomDetails(callback);
}

// Yeah, I know... This isn't really HipChat.
// Didn't want to make an AUI helper though. 😚
export function showAUIFlag(type, title, body) {
    AJS.flag({
        type: type,
        title: title,
        body: body,
        close: 'auto'
    });
}