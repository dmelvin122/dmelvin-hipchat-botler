import React from 'react';
import cx from 'classnames';
import _ from 'lodash';

const buttonClass = 'aui-button';

export default class ServiceAttributeButtons extends React.Component {
    
    _handleOnClick(e) {
        e.preventDefault();
        this.props.appender(e.target.dataset.id);
    }

    getButton(name, id) {
        return (
            <button onClick={e => {this._handleOnClick(e)}} className={buttonClass} key={id} data-id={id}>{name}</button>
        )
    }

    getButtons() {
        return this.props.attributes.map(attr => {
            return this.getButton(attr.display, attr.id);
        })
    }
    
    render() {
        return (
            <div className="aui-buttons">
                { this.getButtons() }
            </div>
        )
    }
}