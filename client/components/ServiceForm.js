import React from 'react';
import cx from 'classnames';
import _ from 'lodash';

import Select2 from 'react-select2-wrapper';
import 'react-select2-wrapper/css/select2.css';

import { MentionsInput, Mention } from 'react-mentions'
import defaultStyle from "../styles/defaultStyle";

import ServiceAttributeButtons from './ServiceAttributeButtons';

import { saveBotlerService, deleteBotlerService, enableDialogButtons } from '../actions/DialogActions';
import { CREATE_SERVICE,
         EDIT_SERVICE,
         DIALOG_PRIMARY_ACTION_KEY, 
         DIALOG_DELETE_ACTION_KEY, 
         DIALOG_CLOSE_ACTION_KEY } from '../actions/DialogOptions';

const defaultService = {
    name: '',
    event: 'room_message',
    pattern: '',
    reply: '',
    replyHtml: false,
    enable: true,
    notify: false
};

export default class ServiceForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            service: props.service || defaultService,
            replyCursor: 0
        }
    }

    componentWillMount() {
        enableDialogButtons(this.props.type === CREATE_SERVICE);
        HipChat.register({
            'dialog-button-click': (event, closeDialog) => {
                if (event.action === DIALOG_PRIMARY_ACTION_KEY) {
                    this.saveService();
                } else if (event.action === DIALOG_DELETE_ACTION_KEY) {
                    this.deleteService();
                } else {
                    closeDialog(true);
                }
            }
        });
    }

    saveService() {
        saveBotlerService(this.state.service);
    }

    deleteService() {
        deleteBotlerService(this.state.service);
    }

    getNameField() {
        return (
            <div className="field-group">
                <label htmlFor="name">Service Name <span className="aui-icon icon-required">(required)</span></label>
                <input className="text long-field" type="text"
                    id="name" name="name" 
                    value={this.state.service.name}
                    onChange={(e) => this._handleNameChange(e)}
                    placeholder="This is not greatest service in the world. This is just a tribute." />
                <div className="description">Give it a nice and meaningful name.</div>
            </div>
        )
    }

    _getEventsList() {
        return Object.keys(this.props.events).map((key, index) => {
            return { text: this.props.events[key].friendlyName, id: key }
        });
    }

    getEventSelect() {
        return (
            <div className="field-group">
                <label htmlFor="event">Event</label>
                <Select2 id="event" name="event"
                    value={this.state.service.event} 
                    className="long-field" style={{width: '50%'}} 
                    onChange={(e) => this._handleEventChange(e)}
                    data={this._getEventsList()}
                />
                <div className="description">What event should Botler look out for?</div>
            </div>
        )
    }

    getPatternField() {
        if (this.state.service.event !== 'room_message') {
            return;
        }
        return (
            <div className="field-group" id="event-params">
                <label htmlFor="pattern">
                    Message Pattern <span className="aui-icon icon-required">(required)</span>
                </label>
                <input  className="text long-field" 
                        type="text"
                        id="pattern" 
                        name="pattern" 
                        placeholder="/slashcommand"
                        value={this.state.service.pattern}
                        onChange={(e) => this._handlePatternChange(e)} />
                <div className="description">
                    What words should Botler look for before triggering the action. <br/>
                    You can use REGEX, e.g. ^/regex|test, matching emoticons? try \(rip\)\B
                </div>
            </div>
        )
    }

    _appendReplyField(value) {
        let toInsert = '{{' + value + '}} ';
        let newState = _.merge({}, this.state);
        let originalReply = newState.service.reply;
        newState.service.reply = `${originalReply.substring(0, this.state.replyCursor)}${toInsert}${originalReply.substring(this.state.replyCursor+1,originalReply.length)}`;
        newState.replyCursor = this.state.replyCursor + toInsert.length;
        this.setState(newState);
        this.replyInput.refs.input.focus();
    }

    getAttributeButtons() {
        const attributes = this.props.events[this.state.service.event].attributes;
        let data = Object.keys(attributes).map((attr, key) => {
            return {
                id: attr,
                display: attributes[attr].friendlyName
            };
        });

        return (
            <ServiceAttributeButtons appender={this._appendReplyField.bind(this)} attributes={data} />
        )
    }

    getReplyField() {
        const attributes = this.props.events[this.state.service.event].attributes;
        let data = Object.keys(attributes).map((attr, key) => {
            return {
                id: attr,
                display: attributes[attr].friendlyName
            };
        });

        data.push({
            id: 'random',
            display: 'Randomiser'
        });

        for (let i = 1; i < 4; i++) {
            data.push({
                id: `capture-${i}`,
                display: `Capture Group ${i}`
            });
        }

        function displayTransform(id, display, type) {
            if (id === 'random') {
                return '[random1, random2, random3]';
            }

            if (id.indexOf('capture') > -1) {
                const captureId = id.split('-')[1];
                return `{{${captureId}}}`;
            }

            return `{{${id}}}`;
        }

        return (
            <div className="field-group">
                <label htmlFor="reply">Reply</label>
                <MentionsInput ref={(input) => this.replyInput = input} onBlur={e => this._handleReplyBlur(e)} value={this.state.service.reply}
                    style={ defaultStyle({ multiLine: true }) }
                    displayTransform={displayTransform}
                    onChange={(e, newValue, newPlainTextValue, mentions) => this._handleReplyChange(e, newValue, newPlainTextValue, mentions)}
                    placeholder="Botler is awesome!">
                    <Mention trigger={'{{'}
                        data={data}
                        appendSpaceOnAdd={true}/>
                </MentionsInput>
                { this.getAttributeButtons() }
                <div className="description">
                    What do you want Botler to say when this happens? <br/>
                    For captured/matched terms from your REGEX, use {'{{$1}}, {{$2}}.. {{$n}}'}. <br/>
                    For a range of random possibilities, use [message one, message two, message n].
                </div>
            </div>
        )
    }

    _handleReplyBlur(e) {
        this.setState(Object.assign({}, this.state, {replyCursor: e.target.selectionStart || 0}));
    }

    getCheckboxes() {
        return (
            <div className="field-group">
                <div className="checkbox">
                    <input className="checkbox" type="checkbox" name="replyHtml" id="replyHtml"
                        onChange={(e) => this._handleReplyHtmlChange(e)} 
                        checked={this.state.service.replyHtml}/>
                    <label htmlFor="replyHtml">HTML</label>
                </div>
                <div className="description">Do you want HTML to be encoded?</div>
                <div className="checkbox">
                    <input className="checkbox" type="checkbox" name="notify" id="notify"
                        onChange={(e) => this._handleNotifyChange(e)} 
                        checked={this.state.service.notify}/> 
                    <label htmlFor="notify">Notify</label>
                </div>
                <div className="description">Do you want Botler to notify everyone in the room?</div>
                <div className="checkbox">
                    <input className="checkbox" type="checkbox" name="enable" id="enable"
                        onChange={(e) => this._handleEnableChange(e)} 
                        checked={this.state.service.enable}/>
                    <label htmlFor="enable">Enabled</label>
                </div>
                <div className="description">Turn this service on?</div>
            </div>
        )
    }

    _handleNameChange(e) {
        this.handleChange('name', e.target.value);
    }

    _handleEventChange(e) {
        const select = e.target;
        const selectedValue = select.item(select.selectedIndex);
        this.handleChange('event', selectedValue.value);
    }

    _handlePatternChange(e) {
        this.handleChange('pattern', e.target.value);
    }

    _handleReplyChange(e, newValue, newPlainTextValue, mentions) {
        this.handleChange('reply', newPlainTextValue);
    }

    _handleReplyHtmlChange(e) {
        this.handleChange('replyHtml', e.target.checked);
    }

    _handleNotifyChange(e) {
        this.handleChange('notify', e.target.checked);
    }

    _handleEnableChange(e) {
        this.handleChange('enable', e.target.checked);
    }

    handleChange(key, newValue) {
        let newState = {
            service: {
                [key]: newValue
            }
        };
        this.setState(_.merge({}, this.state, newState));
    }

    render() {
        return (
            <div>
                {this.props.backButton}
                <form className="aui" id="new-botler-service">
                    { this.getNameField() }
                    { this.getEventSelect() }
                    { this.getPatternField() }
                    { this.getReplyField() }
                    { this.getCheckboxes() }
                </form>
            </div>
        )
    }
}