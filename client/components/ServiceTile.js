import React from 'react';
import cx from 'classnames';
import CopyToClipboard from 'react-copy-to-clipboard';

import { openEditDialog } from '../actions/DialogActions';

function getAbotarImage(id, encodedName) {
    return `http://abotars.hipch.at/bot/botler-${id}-${encodedName}`;
}

const defaultState = {
    showDetails: false,
    copied: false,
};

export default class ServiceTile extends React.Component {

    constructor(props) {
        super(props);
        this.state = defaultState;
    }

    _handleClickService() {
        openEditDialog(this.props.id);
    }

    _serviceToJSON() {
        return {
            name: this.props.name,
            event: this.props.event,
            pattern: this.props.pattern,
            reply: this.props.reply,
            replyHtml: this.props.replyHtml,
            notify: this.props.notify,
            enable: this.props.enable
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState(defaultState);
    }

    getAvatar() {
        const classnames = cx({
            'botler-service-img': true,
            'botler-dead': !this.props.enable
        });

        const style = {
            cursor: 'pointer'
        }

        return (
            <span style={style} onClick={this._handleClickService.bind(this)}>
                <img className={classnames}
                    src={getAbotarImage(this.props.id, this.props.encodedName)} />
            </span>
        );
    }

    getInfo() {
        const style = {
            cursor: 'pointer'
        };

        return (
            <span style={style} className="botler-service-info">
                <a href="#" onClick={this._handleClickService.bind(this)}>{this.props.name}</a>
            </span>
        );
    }

    getDetailsCaret() {
        const style = {
            float: 'right',
            cursor: 'pointer'
        };

        const classnames = cx({
            'aui-icon': true,
            'aui-icon-small': true,
            'aui-iconfont-arrows-down': !this.state.showDetails,
            'aui-iconfont-arrows-up': this.state.showDetails
        });

        return (
            <span onClick={this._onDetailCaretClick.bind(this)}
                style={style}
                id={`botler-arrow-${this.props.id}`}
                className={classnames}>
            </span>
        );
    }

    _onDetailCaretClick() {
        this.setState({
            showDetails: !this.state.showDetails,
            copied: false,
        });
    }

    _appendMentionName(name) {
        HipChat.chat.appendMessage(name);
    }


    getOwners() {
        return Object.keys(this.props.owners).map((key) => {
            const mentionName = `@${this.props.owners[key].mentionName}`;
            return (
                <a key={key} style={{cursor: 'pointer'}} onClick={(e) => this._appendMentionName(mentionName)} className="botler-service-usermention">{mentionName}</a>
            );
        });
    }

    getPatternTr() {
        if (!this.props.pattern || this.props.pattern === '') {
            return;
        }
        return (
            <tr>
                <td><b>Pattern</b></td>
                <td>{this.props.pattern}</td>
            </tr>
        )
    }

    getDetails() {
        if (!this.state.showDetails) {
            return;
        }

        return (
            <div className="botler-service-details" id={`botler-details-${this.props.id}`}>
                <table className="aui">
                    <tbody>
                        <tr>
                            <td><b>Event</b></td>
                            <td>{this.props.event}</td>
                        </tr>
                        {this.getPatternTr()}
                        <tr>
                            <td><b>Trigger Count</b></td>
                            <td>{this.props.triggerCount}</td>
                        </tr>
                        <tr>
                            <td><b>Owners</b></td>
                            <td>
                                {this.getOwners()}
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="2" >
                                {this.getExportLink()}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }

    getExportLink() {
        const containerStyle = {
            display: 'flex',
            justifyContent: 'center',
        }
        const buttonText = this.state.copied ? 'Copied to clipboard!' : 'Export service';
        return (<div style={containerStyle}>
            <CopyToClipboard
                text={JSON.stringify(this._serviceToJSON())}
                onCopy={() => (this.setState({copied: true}))}
            >
                <button disabled={this.state.copied}>{buttonText}</button>
            </CopyToClipboard>
        </div>);
    }

    render() {
        return (
            <div className="botler-service-tile">
                {this.getAvatar()}
                {this.getInfo()}
                {this.getDetailsCaret()}
                {this.getDetails()}
            </div>
        )
    }
}