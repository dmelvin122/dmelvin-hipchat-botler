import React from 'react';
import { removeButtons } from '../../actions/DialogActions';

const imgStyle = {
    width: 'auto',
    height: '250px',
    cursor: 'pointer'
};

export default class TypeSelection extends React.Component {

    componentWillMount() {
        removeButtons();
    }

    handleClick(flow) {
        this.props.flowSetter(flow);
    }

    renderOption(name, image, caption) {
        return (
            <div className="aui-item" key={name}>
                <img style={imgStyle} src={image} onClick={() => this.handleClick(name)} />
                <p>
                    <a style={{cursor: 'pointer'}} onClick={() => this.handleClick(name)}>{caption}</a>
                </p>
            </div>
        )
    }

    render() {
        return (
            <div className="text-center">
                <div className="aui-group">
                    {this.renderOption('create', '/img/new_service.png', 'Create a new service')}
                    {this.renderOption('import', '/img/import.png', 'Import')}
                </div>
            </div>
        )
    }
}