import React from 'react';
import { MentionsInput, Mention } from 'react-mentions'
import defaultStyle from "../../../styles/defaultStyle";
import ServiceAttributeButtons from '../../ServiceAttributeButtons';
import replaceAll from 'replaceall';
import randexp from 'randexp';
import { updatePrimaryAction } from '../../../actions/HipChatActions';
import { DIALOG_CREATE_SAVE_KEY } from '../../../actions/DialogOptions';

export default class Step3 extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            replyCursor: 0
        };

        updatePrimaryAction({
            name: 'Save',
            key: DIALOG_CREATE_SAVE_KEY
        });

        this.validate(props.service.reply);

        let exampleMessage;

        switch (this.props.service.event) {
            case 'room_message':
                exampleMessage = new randexp(this.props.service.pattern).gen();
                break;
            case 'room_enter':
                exampleMessage = `${this.props.user.name} has joined the room.`;
                break;
            case 'room_exit':
                exampleMessage = `${this.props.user.name} has left the room.`;
                break;
            case 'room_file_upload':
                exampleMessage = `botler.png`;
                break;
            case 'room_topic_change':
                exampleMessage = `${this.props.user.name} changed the topic to: Botler is awesome!`;
                break;
        }

        this.state.exampleMessage = exampleMessage;
    }

    validate(reply) {
        updatePrimaryAction({
            enabled: reply.length > 0
        })
    }

    _replaceReplyWithExample(message) {
        let reply = this.props.service.reply;

        reply = replaceAll('{{senderName}}', this.props.user.name, reply);
        reply = replaceAll('{{senderMentionName}}', `@${this.props.user.mention_name}`, reply);
        reply = replaceAll('{{mentions}}', '@Botler @HipChat', reply);
        reply = replaceAll('{{message}}', message, reply);
        reply = replaceAll('{{roomName}}', this.props.room.name, reply);
        
        reply = replaceAll('{{topic}}', 'Botler is awesome!', reply);

        reply = replaceAll('{{fileName}}', 'botler.png', reply);
        reply = replaceAll('{{fileSize}}', '1337', reply);
        reply = replaceAll('{{fileUrl}}', ' https://s3.amazonaws.com/uploads.hipchat.com/123/abc/botler.png', reply);
        reply = replaceAll('{{fileThumbnail}}', 'https://s3.amazonaws.com/uploads.hipchat.com/123/abc/botler_thumb.png', reply);

        return reply;
    }

    _handleChange(name, value) {
        this.props.serviceUpdater(_.merge({}, this.props.service, { [name]: value }));
    }

    _handleReplyChange(e, newValue, newPlainTextValue, mentions) {
        this._handleChange('reply', newPlainTextValue);
        this.validate(newPlainTextValue);
    }

    _handleReplyHtmlChange(e) {
        this._handleChange('replyHtml', e.target.checked);
    }

    _handleNotifyChange(e) {
        this._handleChange('notify', e.target.checked);
    }

    _handleEnableChange(e) {
        this._handleChange('enable', e.target.checked);
    }

    _appendReplyField(value) {
        let toInsert = '{{' + value + '}} ';
        let originalReply = this.props.service.reply;
        const newReply = `${originalReply.substring(0, this.state.replyCursor)}${toInsert}${originalReply.substring(this.state.replyCursor + 1, originalReply.length)}`;
        this._handleChange('reply', newReply);
        let newReplyCursor = this.state.replyCursor + toInsert.length;
        this.setState({ replyCursor: newReplyCursor });
        this.replyInput.refs.input.focus();
        this.validate(newReply);
    }

    getAttributeButtons() {
        const attributes = this.props.events[this.props.service.event].attributes;
        let data = Object.keys(attributes).map((attr, key) => {
            return {
                id: attr,
                display: attributes[attr].friendlyName
            };
        });

        return (
            <ServiceAttributeButtons appender={this._appendReplyField.bind(this)} attributes={data} />
        )
    }

    getReplyField() {
        const attributes = this.props.events[this.props.service.event].attributes;
        let data = Object.keys(attributes).map((attr, key) => {
            return {
                id: attr,
                display: attributes[attr].friendlyName
            };
        });

        data.push({
            id: 'random',
            display: 'Randomiser'
        });

        for (let i = 1; i < 4; i++) {
            data.push({
                id: `capture-${i}`,
                display: `Capture Group ${i}`
            });
        }

        function displayTransform(id, display, type) {
            if (id === 'random') {
                return '[random1, random2, random3]';
            }

            if (id.indexOf('capture') > -1) {
                const captureId = id.split('-')[1];
                return `{{${captureId}}}`;
            }

            return `{{${id}}}`;
        }

        return (
            <div className="text-center" style={{ padding: '0 100px 0 100px' }}>
                <MentionsInput ref={(input) => this.replyInput = input} onBlur={e => this._handleReplyBlur(e)} value={this.props.service.reply}
                    style={defaultStyle({ multiLine: true })}
                    displayTransform={displayTransform}
                    onChange={(e, newValue, newPlainTextValue, mentions) => this._handleReplyChange(e, newValue, newPlainTextValue, mentions)}
                    placeholder="Botler is awesome!">
                    <Mention trigger={'{{'}
                        data={data}
                        appendSpaceOnAdd={true} />
                </MentionsInput>
                {this.getAttributeButtons()}
                <div className="description">
                    {`Type {{ to bring up the suggestions.`}
                </div>
            </div>
        )
    }

    _handleReplyBlur(e) {
        this.setState(Object.assign({}, this.props, { replyCursor: e.target.selectionStart || 0 }));
    }

    getCheckboxes() {
        return (
            <div className="aui-group">
                <div className="aui-item">
                    <input type="checkbox" name="replyHtml" id="replyHtml"
                        onChange={(e) => this._handleReplyHtmlChange(e)}
                        checked={this.props.service.replyHtml} />
                    <label htmlFor="replyHtml">HTML</label>
                    <div className="description">Do you want HTML to be encoded?</div>
                </div>

                <div className="aui-item">
                    <input type="checkbox" name="notify" id="notify"
                        onChange={(e) => this._handleNotifyChange(e)}
                        checked={this.props.service.notify} />
                    <label htmlFor="notify">Notify</label>
                    <div className="description">Do you want Botler to notify everyone in the room?</div>
                </div>

                <div className="aui-item">
                    <input type="checkbox" name="enable" id="enable"
                        onChange={(e) => this._handleEnableChange(e)}
                        checked={this.props.service.enable} />
                    <label htmlFor="enable">Enabled</label>
                    <div className="description">Turn this service on?</div>
                </div>
            </div>
        );
    }

    renderExample() {
        const exampleReply = this._replaceReplyWithExample(this.state.exampleMessage);

        return (
            <div>
                <div className="hc-chat-row hc-msg-notification hc-classic-neue">
                    <div className="hc-chat-from no-action">
                        <img className="aui-avatar aui-avatar-medium aui-avatar-project" src={this.props.user.photo_url} />
                    </div>
                    <div className="hc-chat-msg">
                        <span className="sender-name"><b>{this.props.user.name}</b></span>
                        <br/>
                        <div className="msg-status msg-confirmed">
                            <div className="notification msg-line">{this.state.exampleMessage}</div>
                        </div>
                    </div>
                </div>
                <div className="hc-chat-row hc-msg-notification hc-classic-neue">
                    <div className="hc-chat-from no-action">
                        <img className="aui-avatar aui-avatar-medium" src="/img/botler.svg" />
                    </div>
                    <div className="hc-chat-msg">
                        <span className="sender-name"><b>Botler</b></span>
                        <br/>
                        <div className="msg-status msg-confirmed">
                            <div className="notification msg-line">{exampleReply}</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="text-center">
                {this.renderExample()}
                <h2>What should Botler say in response?</h2>
                <form className="aui" onSubmit={(e) => e.preventDefault()}>
                    {this.getReplyField()}
                    {this.getCheckboxes()}
                </form>
            </div>
        )
    }
}