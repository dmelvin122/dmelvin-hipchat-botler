import React from 'react';
import _ from 'lodash';
import { updatePrimaryAction } from '../../../actions/HipChatActions';
import { DIALOG_CREATE_FLOW_NEXT_KEY } from '../../../actions/DialogOptions';

function getAbotarImage(name) {
    if (!name) {
        return '/img/botler_icon.svg';
    }
    return `http://abotars.hipch.at/bot/botler-${name.replace(/\/|%/g, '-')}`;
}

const avatarStyle = {
    borderRadius: '50%',
    height: '150px'
};

export default class Step1 extends React.Component {

    constructor(props) {
        super(props);

        updatePrimaryAction({
            name: 'Next',
            key: DIALOG_CREATE_FLOW_NEXT_KEY
        });

        this._validate(props.service.name);
    }

    _handleChange(e) {
        this.props.serviceUpdater(_.merge({}, this.props.service, { name: e.target.value }));
        this._validate(e.target.value);
    }

    _validate(name) {
        updatePrimaryAction({
            enabled: name.length > 0
        });
    }

    renderAvatar() {
        return (
            <span>
                <img style={avatarStyle}
                    src={getAbotarImage(this.props.service.name)} />
            </span>
        );
    }

    renderForm() {
        return (
            <form className="aui" id="new-botler-service" onSubmit={(e) => e.preventDefault()}>
                <input className="text long-field" type="text"
                        id="name" name="name"
                        value={this.props.service.name}
                        onChange={(e) => this._handleChange(e)}
                        placeholder="This is not greatest service in the world. This is just a tribute." />                    
                <div className="description">Give it a nice and meaningful name.</div>
            </form>
        );
    }

    render() {
        return (
            <div className="text-center">
                {this.renderAvatar()}
                <h2>What do you want this service to called?</h2>
                {this.renderForm()}
            </div>
        )
    }
}