import React from 'react';
import Select2 from 'react-select2-wrapper';
import 'react-select2-wrapper/css/select2.css';
import { updatePrimaryAction } from '../../../actions/HipChatActions';
import { DIALOG_CREATE_FLOW_NEXT_KEY } from '../../../actions/DialogOptions';

export default class Step2 extends React.Component {

    constructor(props) {
        super(props);

        updatePrimaryAction({
            name: 'Next',
            key: DIALOG_CREATE_FLOW_NEXT_KEY
        });

        this._validate(props.service.pattern);
    }

    _getEventsList() {
        return Object.keys(this.props.events).map((key, index) => {
            return { text: this.props.events[key].friendlyName, id: key }
        });
    }

    _validate(pattern) {
        let enabled = true;

        if (this.props.service.event === 'room_message' && !pattern.length) {
            enabled = false;
        }

        updatePrimaryAction({
            enabled  
        });
    }

    _handleEventChange(e) {
        const select = e.target;
        const selectedValue = select.item(select.selectedIndex);
        this.props.serviceUpdater(_.merge({}, this.props.service, { event: selectedValue.value }));
        this._validate(this.props.service.pattern);        
    }

    getEventImage() {
        return <img style={{height: '175px'}} src={`/img/${this.props.service.event}.png`}/>;
    }

    getEventSelect() {
        return (
            <div>
                <Select2 id="event" name="event"
                    value={this.props.service.event} 
                    className="botler-select2" 
                    data={this._getEventsList()}
                    onChange={(e) => this._handleEventChange(e)}
                />

            </div>
        )
    }

    _handlePatternChange(e) {
        this.props.serviceUpdater(_.merge({}, this.props.service, { pattern: e.target.value }));
        this._validate(e.target.value);
    }

    getPatternField() {
        if (this.props.service.event !== 'room_message') {
            return;
        }
        return (
            <div style={{marginTop: '20px'}}>
                <input  className="text long-field" 
                        type="text"
                        id="pattern" 
                        name="pattern" 
                        placeholder="What phrase or pattern should trigger this service?"
                        value={this.props.service.pattern}
                        onChange={(e) => this._handlePatternChange(e)} />
                <div className="description">
                    <p>You can use REGEX, e.g. ^/regex|test, matching emoticons? try \(rip\)\B</p>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div className="text-center">
                <h2>What event should Botler look out for?</h2>
                {this.getEventImage()}
                <form className="aui" onSubmit={(e) => e.preventDefault()}>
                    {this.getEventSelect()}
                    {this.getPatternField()}
                </form>
            </div>
        )
    }
}