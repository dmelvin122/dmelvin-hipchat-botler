import React from 'react';
import { updateHipChatDialog } from '../../../actions/HipChatActions';
import { saveBotlerService } from '../../../actions/DialogActions';
import * as DialogOptions from '../../../actions/DialogOptions';

import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import Step4 from './Step4';

const defaultService = Object.freeze({
    name: '',
    event: 'room_message',
    pattern: '',
    reply: '',
    replyHtml: false,
    enable: true,
    notify: false
});

export default class CreateContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            step: 1,
            service: defaultService
        };
    }

    componentWillMount() {
        updateHipChatDialog(DialogOptions.CREATE_FLOW_BUTTONS);

        HipChat.register({
            'dialog-button-click': (event, closeDialog) => {
                // define these in dialogoptions
                if (event.action === DialogOptions.DIALOG_CREATE_FLOW_NEXT_KEY) {
                    this._goNext();
                } else if (event.action === DialogOptions.DIALOG_CREATE_FLOW_BACK_KEY) {
                    this._goBack();
                } else if (event.action === DialogOptions.DIALOG_CREATE_SAVE_KEY) {
                    saveBotlerService(this.state.service);
                } else {
                    closeDialog(true);
                }
            }
        });
    }

    _goNext() {
        const step = this.state.step + 1;
        this.setState({step});
    }

    _goBack() {
        const step = this.state.step - 1;
        if (step === 0) {
            this.props.flowSetter('init');
        }
        this.setState({step});
    }

    _updateService(service) {
        this.setState({service});
    }

    renderStep() {
        const serviceUpdater = (service) => this._updateService(service);

        switch(this.state.step) {
            case 1: // name
                return <Step1 service={this.state.service} serviceUpdater={serviceUpdater} />
            case 2: // event and pattern
                return <Step2 service={this.state.service} events={this.props.events} serviceUpdater={serviceUpdater} />
            case 3: // reply, notify, html
                return <Step3 service={this.state.service} events={this.props.events} user={this.props.user} room={this.props.room} serviceUpdater={serviceUpdater} />
            case 4: // review
                return <Step4 service={this.state.service} serviceUpdater={serviceUpdater} />
            default:
                return null;                    
        }
    }

    render() {
        return (
            <div>
                {this.renderStep()}
            </div>
        )
    }
}