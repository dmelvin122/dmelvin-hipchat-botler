import React from 'react';
import { showAUIFlag } from '../../../actions/HipChatActions';
import ServiceForm from '../../ServiceForm';
import _ from 'lodash';

const dbShowAUIFlag = _.debounce(showAUIFlag, 500);

export default class ImportContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            importDescriptor: null
        }
    }

    _handleChange(e) {
        const blob = e.target.value;

        try {
            let importDescriptor = JSON.parse(blob);
            this.setState({ importDescriptor });
        } catch (e) {
            dbShowAUIFlag('error', 'Invalid service descriptor.', 'Please make sure you use a exported Botler service.');
        }
    }

    renderForm() {
        return (
            <form className="aui" id="new-botler-service">
                <textarea className="textarea long-field" name="service-import" style={{height: '220px'}}
                    value={this.state.importDescriptor ? JSON.stringify(this.state.importDescriptor): ''}
                    onChange={(e) => this._handleChange(e)}
                    id="service-import" placeholder="Paste your service descriptor blob here."></textarea>
                <div className="description">Import a service</div>
            </form>
        );
    }


    render() {

        if (this.state.importDescriptor) {
            return <ServiceForm {...this.props} service={this.state.importDescriptor} />;
        } else {
            return (
                <div className="text-center">
                    <h2>Paste the exported Botler Service here.</h2>
                    {this.renderForm()}
                </div>
            )
        }
    }

}