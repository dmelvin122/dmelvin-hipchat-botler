import React from 'react';
import cx from 'classnames';
import _ from 'lodash';

export default class ServiceFilter extends React.Component {

    handleOnChange(e) {
        this.props.filterer(e.target.value);
    }

    render() {
        return (
            <form className="aui" onSubmit={e => e.preventDefault()}>
                <input className="text" type="text" placeholder="Search for a service..." onChange={e => this.handleOnChange(e)}/>
            </form>
        );
    }
}