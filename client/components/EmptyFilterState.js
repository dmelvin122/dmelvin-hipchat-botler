import React from 'react';

export default class ServiceAttributeButtons extends React.Component {
    render() {
        return (
            <div className="text-center" id="botler-sidebar-empty">
                <img id="spin" src="/img/botler.svg" />
                <p>
                    Could not find any services (feelsbadman).
                </p>
                <p>
                    Try searching by the service name, pattern, reply or name of the user who created the service.
                </p>
            </div>
        )
    }
}