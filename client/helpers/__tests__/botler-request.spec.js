import * as BotlerRequests from '../botler-requests';
import * as superagent from 'superagent';

describe('Botler request helpers', () => {

    const hipChatBearerToken = '12hjsfgsjio134joierwdldsf';

    beforeEach(() => {
        let HipChat = {
            auth: {},
            user: {}
        };

        HipChat.auth.withToken = jasmine.createSpy('withToken').and.callFake((callback) => {
            callback(false, hipChatBearerToken);
        });

        HipChat.user.getCurrentUser = jasmine.createSpy('getCurrentUser').and.callFake((callback) => {
            callback(false, {
                id: 123,
                name: 'some name',
                mention: '@mention'
            });
        });        

        window.HipChat = HipChat;
    })

    describe('getServiceById', () => {
        it('should call superagent with the correct params', (done) => {
            const id = 123;
            BotlerRequests.getServiceById(id, (err, res) => {
                expect(res.req.url).toEqual(`/botler/room_service?id=${id}`);
                expect(res.req.method).toEqual('GET');
                expect(res.req.header.Authorization).toEqual(`JWT ${hipChatBearerToken}`);
                done();
            });
        });
    });

    describe('saveService', () => {
        it('should call superagent with the correct params', (done) => {
            const service = {
                name: 'test'
            };
            BotlerRequests.saveService(service, (err, res) => {
                expect(res.req.url).toEqual(`/botler/room_service`);
                expect(res.req.method).toEqual('POST');
                expect(res.req.header.Authorization).toEqual(`JWT ${hipChatBearerToken}`);
                expect(res.req._data.name).toEqual(service.name);
                expect(res.req._data.owners).toBeDefined();
                done();
            });
        });

        it('should call superagent with the correct params when service id is present', (done) => {
            const service = {
                id: 123,
                name: 'test'
            };
            BotlerRequests.saveService(service, (err, res) => {
                expect(res.req.url).toEqual(`/botler/room_service`);
                expect(res.req.method).toEqual('PUT');
                expect(res.req.header.Authorization).toEqual(`JWT ${hipChatBearerToken}`);
                expect(res.req._data.id).toEqual(service.id);
                expect(res.req._data.name).toEqual(service.name);
                expect(res.req._data.owners).toBeDefined();
                expect(res.req._data.newOwner).toBeDefined();
                done();
            });
        });
    });
    
    describe('deleteService', () => {
        it('should call superagent with the correct params', (done) => {
            const id = 123;
            BotlerRequests.deleteService(id, (err, res) => {
                expect(res.req.url).toEqual(`/botler/room_service`);
                expect(res.req.method).toEqual('DELETE');
                expect(res.req.header.Authorization).toEqual(`JWT ${hipChatBearerToken}`);
                expect(res.req._data.id).toEqual(id);
                done();
            });
        });
    });
});