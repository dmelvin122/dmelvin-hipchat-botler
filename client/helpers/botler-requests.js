import request from 'superagent';
import _ from 'lodash';

const SERVICE_URL = '/botler/room_service';

export function getServiceById(id, callback) {
    HipChat.auth.withToken((err, token) => {
        if (!err) {
            request
                .get(SERVICE_URL)
                .query({ id: id })
                .set('Authorization', `JWT ${token}`)
                .end(callback)
        }
    })   
}

function createService(service, token, callback) {
    request
        .post(SERVICE_URL)
        .set('Authorization', `JWT ${token}`)
        .set('Content-Type', 'application/json')
        .send(service)
        .end(callback)
}

function updateService(service, user, token, callback) {
    // legacy bullshit. Should get rid of this stuff.
    service.newOwner = JSON.stringify({
        id: user.id, name: user.name, mentionName: user.mention_name});
    request
        .put(SERVICE_URL)
        .set('Authorization', `JWT ${token}`)
        .set('Content-Type', 'application/json')
        .send(service)
        .end(callback)
}

export function saveService(service, callback) { 
    HipChat.user.getCurrentUser((err, user) => {
        if (err) {
            callback(false)
        }

        HipChat.auth.withToken((err, token) => {
            let newService = _.merge({}, service);

            newService.owners = JSON.stringify(_.merge({}, service.owners, {
                [user.id]: {
                    name: user.name,
                    mentionName: user.mention_name
                }
            }));

            if (newService.id) {
                updateService(newService, user, token, callback);
            } else {
                createService(newService, token, callback);
            }
            
        });
    });
}

export function deleteService(serviceId, callback) {
    HipChat.auth.withToken((err, token) => {
        request
            .delete(SERVICE_URL)
            .set('Authorization', `JWT ${token}`)
            .set('Content-Type', 'application/json')
            .send({id: serviceId})
            .end(callback)
    })
}