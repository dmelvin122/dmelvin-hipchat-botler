import React from 'react';
import _ from 'lodash';
import ServiceTile from '../components/ServiceTile';
import ServiceFilter from '../components/ServiceFilter';
import EmptyFilterState from '../components/EmptyFilterState';
import { checkAllJSPresent, EVENT_SIDEBAR_UPDATE } from '../actions/HipChatActions';
import { openCreateDialog } from '../actions/DialogActions';
import Fuse from 'fuse.js';

const FUSE_OPTIONS = {
    shouldSort: true,
    threshold: 0.4,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    keys: [
        "name",
        "reply",
        "pattern",
        "ownersQuery"
    ]
};

export default class Sidebar extends React.Component {

    constructor(props) {
        super(props);
        checkAllJSPresent();
        this.state = window.__PRELOADED_STATE__;
        HipChat.register({
            [EVENT_SIDEBAR_UPDATE]: (data) => {
                let newState = _.merge({}, this.state);
                newState.roomServices = data.roomServices;
                this.setState(newState);
            }
        });
    }

    handleOnFilter(value) {
        this.setState(_.merge({}, this.state, { filter: value }));
    }

    getServiceFilter() {
        return (
            <ServiceFilter filterer={this.handleOnFilter.bind(this)} />
        );
    }

    getServiceTiles() {
        let data = this.state.roomServices;
        if (this.state.filter) {
            let fuse = new Fuse(this.state.roomServices, FUSE_OPTIONS);
            data = fuse.search(this.state.filter)
        }

        if (!data.length) {
            return (
                <EmptyFilterState />
            );
        }

        return data.map((roomService, key) => {
            return (
                <ServiceTile {...roomService} key={key} />
            );
        });
    }

    getFooter() {
        return (
            <div id="footer">
                <div id="botler-button-wrapper">
                    <button className="aui-button aui-button-link" onClick={this._handleOpenNewDialog}>
                        <span className="aui-icon aui-icon-small aui-iconfont-add">New Service</span> New Service
                    </button>
                </div>
            </div>
        )
    }

    _handleOpenNewDialog() {
        openCreateDialog();
    }

    renderEmptyState() {
        return (
            <div className="aui-group sidebar">
                <div className="text-center" id="botler-sidebar-empty">
                    <img src="/img/botler.svg" />
                    <button className="aui-button aui-button-primary" id="botler-empty-add-new-service" onClick={this._handleOpenNewDialog}>
                        <span className="aui-icon aui-icon-small aui-iconfont-add"></span> Add a service
                    </button>
                </div>
            </div>
        )
    }

    render() {
        if (!this.state.roomServices.length) {
            return this.renderEmptyState();
        }

        return (
            <div className="sidebar">
                <div id="botler-service-filter">
                    {this.getServiceFilter()}
                </div>
                <div id="botler-services">
                    {this.getServiceTiles()}
                </div>
                {this.getFooter()}
            </div>
        )
    }
}