import React from 'react';
import _ from 'lodash';
import { getServiceById } from '../helpers/botler-requests';
import { checkAllJSPresent, getCurrentUser, getRoomDetails } from '../actions/HipChatActions';
import { enableDialogButtons, disableDialogButtons } from '../actions/DialogActions';
import { CREATE_SERVICE, 
         EDIT_SERVICE } from '../actions/DialogOptions';
import ServiceForm from '../components/ServiceForm';
import TypeSelection from '../components/ServiceFlow/TypeSelection';
import CreateContainer from '../components/ServiceFlow/Create/CreateContainer';
import ImportContainer from '../components/ServiceFlow/Import/ImportContainer';

export default class Dialog extends React.Component {
    constructor(props) {
        super(props);
        checkAllJSPresent();
        this.state = window.__PRELOADED_STATE__;
        
        getCurrentUser((err, user) => {
            this.setState({user: user});
        });
        
        getRoomDetails((err, room) => {
            this.setState({room: room});
        });

        HipChat.register({
            "receive-parameters": (parameters) => {
                this.setDialogParameters(parameters);
            }
        });
    }

    setDialogParameters(parameters) {
        if (parameters && parameters.type) {
            if (parameters.type === CREATE_SERVICE) {
                this.setState(_.merge({},this.state, { type: CREATE_SERVICE, flow: 'init' }));
            } else {
                if (parameters.serviceId) {
                    getServiceById(parameters.serviceId, (err, res) => {
                        let newState;
                        if (!err) {
                             newState = { 
                                type: EDIT_SERVICE,
                                service: res.body 
                            };
                        } else {
                            newState = { type: CREATE_SERVICE, flow: 'init' }
                        }
                        this.setState(_.merge({}, this.state, newState));
                    })
                }
            }
        }
    }

    renderLoading() {
        return (
            <div className="aui-dialog2-content">
            </div>
        )
    }

    setFlow(flow) {
        this.setState({ flow });
    }

    renderForm() {
        const flowSetter = (flow) => this.setFlow(flow);

        const backButton = <button className="aui-button" onClick={() => this.setFlow('init')}>Back</button>;

        if (this.state.type === CREATE_SERVICE) {
            switch (this.state.flow) {
                case 'create':
                    return <CreateContainer {...this.state} flowSetter={flowSetter} />
                case 'import':
                    return <ImportContainer {...this.state} flowSetter={flowSetter} />
                case 'ifttt':
                    return null;
                case 'integration':
                    return null;
                default:
                    return <TypeSelection flowSetter={flowSetter} />;
            }
        } else {
            return <ServiceForm {...this.state} />; 
        }
    }

    render() {
        if (!this.state.type) {
            return this.renderLoading();
        }

        return (
            <div className="aui-dialog2-content">
                {this.renderForm()}
            </div>
        )
        
    }    
}