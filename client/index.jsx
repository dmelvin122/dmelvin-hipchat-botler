import React from 'react'
import { render } from 'react-dom'
import { Router, Route, Link, browserHistory } from 'react-router'

import Dialog from './containers/Dialog';
import Sidebar from './containers/Sidebar';

render((
  <Router history={browserHistory}>
    <Route path="/dialog" component={Dialog}/>
    <Route path="/sidebar" component={Sidebar}/>
  </Router>
), document.getElementById('root'))