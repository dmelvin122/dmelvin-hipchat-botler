import React, { PureComponent } from 'react';
import CupcakeIpsum from '../modules/CupcakeIpsum.jsx';

export default class Support extends PureComponent {
  render() {
    return (
      <div>
        <h1>Support</h1>
        <h2>Need help using Botler?</h2>
        <p>
          Feel free to ask a question on the <a href="https://answers.atlassian.com/questions/topics/38242195/addon-botler">Atlassian Answers <b>addon-botler</b> topic</a>.
        </p>

        <h2>Found a bug?</h2>
        <p>Firstly, sorry about that! Botler is a side project for some Atlassians so we don't get much time to work on it.</p>
        <p>Raise a ticket on the <a href="https://bitbucket.org/khanhfucius/hipchat-botler/issues/new">Bitbucket Issue Tracker</a> and we'll try to fix it when we can.</p>

        <h2>Got ideas for Botler?</h2>
        <p>If you've got an neat or spicy ideas that would you love to see on Botler, feel free to raise a feature request on <a href="https://bitbucket.org/khanhfucius/hipchat-botler">Bitbucket</a>, or heck, raise a pull request!</p>
      </div>
    );
  }
}
