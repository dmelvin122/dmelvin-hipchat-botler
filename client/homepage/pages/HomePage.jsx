import React, { PureComponent } from 'react';
import Button from 'ak-button';
import Avatar from 'ak-avatar';
import CupcakeIpsum from '../modules/CupcakeIpsum.jsx';

const testimonialDivStyle = {
  display: 'flex',
  padding: '25px'
};

const avatarStyle = {
  width: '150px',
  borderRadius: '50%'
};

export default class HomePage extends PureComponent {
  constructor() {
    super();
    this.state = {
      isOpen: false
    }
  }

  showModal = () => {
    this.setState({ isOpen: true });
  }

  hideModal = () => {
    this.setState({ isOpen: false });
  }

  render() {
    return (
      <div>
        <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
          <h1>HipChat Botler</h1>
          <p>
            <Button
              appearance="primary"
              href="https://www.hipchat.com/addons/install?url=https://hipchat-botler.useast.atlassian.io/atlassian-connect.json"
              >Add to HipChat</Button>
          </p>
        </div>

        <div style={{ display: 'flex', justifyContent: 'space-around' }}>
          <img src="/img/botler.svg" />
        </div>

        <p>
          Yo dawg, I heard you liked HipChat bots, so we're letting make HipChat bots with this HipChat bot.
          <img src="/img/yodawg.png" style={{ width: '30px' }} />
        </p>

        <p>HipChat bots are fun, annoying and useful little things that make our chatrooms more than just regular chatrooms. The process of making a HipChat bot is quite simple with BYO, the HipChat REST API, and HipChat Connect, that is, <strong>if you're a developer</strong>. What about all the non-technical HipChatters out there?</p>
        <p>Enter Botler, your very own bot butler.</p>

        <h2>Services</h2>

        <div style={{ padding: '25px' }}>
          <img src="/img/botler-create.gif" style={{width: '100%'}}/>
        </div>

        <p>Create "room services" with a classic form in popup dialog. Just select what events and terms you want Botler to listen to and then enter what you want Botler to say whent that happens.</p>
        <p>For you advanced users, you can use regular expression for smarter phrase detection and even use captured terms in your replies.</p>
        <p>By default, Botler's replies will be rendered just like your normal messages, which means that your @mentions and emoticons will appear as expected. You can opt-in to use HTML rendering instead if you're a fancy hypertext wizard.</p>

        <br />
        <hr />

        <h2>Testimonials</h2>
        <br />
        <div style={testimonialDivStyle}>
          <Avatar size="xlarge" src="/img/paul.png" />
          <blockquote>
            <p>I had a happy life. Now I spent all my time trying to come up with Botler commands someone might notice and find funny at least one time before it gets deleted.</p>
            <cite>Paul Thompson, Docker God, Atlassian</cite>
          </blockquote>
        </div>
        <div style={testimonialDivStyle}>
          <Avatar size="xlarge" src="/img/sami.png" />
          <blockquote>
            <p>Thanks to Botler, I realised that I'm awful at regex.</p>
            <cite>Sami Peachey, Room Admin, Atlassian</cite>
          </blockquote>
        </div>
        <div style={testimonialDivStyle}>
          <Avatar size="xlarge" src="/img/saxon.png" />
          <blockquote>
            <p>Saved my life. 9/10</p>
            <cite>Saxon Bruce, New Zealander, Atlassian</cite>
          </blockquote>
        </div>
        <div style={testimonialDivStyle}>
          <Avatar size="xlarge" src="/img/alec.png" />
          <blockquote>
            <p>Increased my memeficiency by 1000%.</p>
            <cite>Alec Posney, Uses Linux, Atlassian</cite>
          </blockquote>
        </div>
        <div style={testimonialDivStyle}>
          <Avatar size="xlarge" src="/img/henry.png" />
          <blockquote>
            <p>Without Botler I'd never have found out which of my colleagues are shitlords.</p>
            <cite>Henry Barnett, has nice shirts, Atlassian</cite>
          </blockquote>
        </div>
      </div>
    );
  }
}
