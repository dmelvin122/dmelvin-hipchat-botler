import React, { PureComponent } from 'react';
import CopyToClipboard from 'react-copy-to-clipboard';
import Button from 'ak-button';

const services = [
  {
    json: { "name": "Food watch", "event": "room_message", "pattern": "^/foodwatch (.*)?", "reply": "* 🚨 * f o o d   w a t c h * 🚨 * \n{{$1}}", "replyHtml": false, "notify": false, "enable": true },
    description: `Alert your team mates when there's food around!`,
    demo: 'foodwatch.png'
  },
  {
    json: { "name": "@all shaming", "event": "room_message", "pattern": "@all", "reply": "{{senderMentionName}} please don't use @ all. A lot of people are lurking in the room, someone will help you eventually.", "replyHtml": false, "notify": false, "enable": true },
    description: `Let people know that it's not cool to use @all in your room.`,
    demo: 'all-shaming.png'
  },
  {
    json: { "name": "Magic 8 Ball", "event": "room_message", "pattern": "^/magic8ball (.*)", "reply": "{{$1}}: [It is certain,It is decidedly so,Without a doubt,Yes Definitely,You may rely on it,As I see it yes,Most likely,Outlook good,Yes,Signs point to yes,Reply hazy try again,Ask again later,Better not tell you now,Cannot predict now,Concentrate and ask again,Don't count on it,My reply is no,My sources say no,Outlook not so good,Very doubtful]", "replyHtml": false, "notify": false, "enable": true },
    description: `Using the randomiser in the reply to create a magic 8 ball!`,
    demo: 'magic8ball.png'
  },
  {
    json: { "name": "hehehe", "event": "room_message", "pattern": "hehehe", "reply": "https://media.giphy.com/media/9MFsKQ8A6HCN2/giphy.gif", "replyHtml": false, "notify": false, "enable": true },
    description: `hehehehehe image links`,
    demo: 'hehehe.png'
  },
  {
    json: { "name": "Roll d20", "event": "room_message", "pattern": "^/roll d20", "reply": "I rolled a [1 (failed),2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20! (success)]", "replyHtml": false, "notify": false, "enable": true },
    description: `roll a d20`,
    demo: 'd20.png'
  },
  {
    json: {"name":"Short straw","event":"room_message","pattern":"/draw straw","reply":"<img src=\"http://lizaamericashost.com/wp-content/uploads/2015/07/straw-red1.jpg\" height=\"[50, 100, 100, 100, 100]\"/>","replyHtml":true,"notify":false,"enable":true},
    description: `Draw straws with HTML and randomiser`,
    demo: 'drawstraw.png'
  },
  {
    json: {"name":"Nerd","event":"room_message","pattern":"\\bnerds?\\b","reply":"https://media.giphy.com/media/OMK7LRBedcnhm/giphy.gif","replyHtml":false,"notify":false,"enable":true},
    description: `Neeeeeeerdddddd!`,
    demo: 'nerd.png'
  },
]

export default class Library extends PureComponent {

  getServices() {
    return services.map((service, index) => {
      return (
        <tr key={index} style={{ borderBottom: '2px solid #DFE1E6' }}>
          <td>
            <img src={`/img/services/${service.demo}`} style={{ width: '250px' }} />
          </td>
          <td>{service.json.name}</td>
          <td>{service.description}</td>
          <td>
            <CopyToClipboard
              text={JSON.stringify(service.json)}
              >
              <Button appearance="default">Copy to clipboard</Button>

            </CopyToClipboard>
          </td>
        </tr>
      )
    })
  }

  render() {
    return (
      <div>
        <h1>Services Library</h1>
        <p>To import a service, just do the following:</p>
        <ul>
          <li>Copy the JSON blob.</li>
          <li>Go to the room that has Botler installed.</li>
          <li>Click <b>New Service</b> in the Botler sidebar panel.</li>
          <li>Select <b>Import</b></li>
          <li>Paste the JSON blob into the textbox.</li>
          <li>???</li>
          <li>Profit! 💸💸💸</li>
        </ul>

        <table>
          <thead>
            <th></th>
          </thead>
          <tbody>

            {this.getServices()}

          </tbody>
        </table>
      </div>
    );
  }
}
