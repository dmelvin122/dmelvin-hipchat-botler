import React, { PureComponent, PropTypes } from 'react';
import Nav, { AkContainerItem, AkContainerHeader as NavHeader } from 'ak-navigation';
import { Link } from 'react-router';
import { akGridSize } from 'akutil-shared-styles';
import AtlassianIcon from 'ak-icon/glyph/atlassian';
import HomeIcon from 'ak-icon/glyph/home';
import HelpIcon from 'ak-icon/glyph/help';
import ProjectIcon from 'ak-icon/glyph/projects';
import 'ak-css-reset';

// Would like to use a LESS file to import styles here,
// but create-react-app doesn't support it.
const gridSizeInt = parseInt(akGridSize, 10);

const myLinks = [
  ['/', 'Home', HomeIcon],
  ['/library', 'Services Library', ProjectIcon],
  ['/support', 'Support', HelpIcon],
];

export default class App extends PureComponent {
  static contextTypes = {
    router: PropTypes.object,
  }

  render() {
    return (
      <div
        style={{
          display: 'flex',
          height: '100vh',
          overflowY: 'auto',
        }}
      >
        <style>{'body { margin: 0 }'}</style>
        <Nav
          containerHeader={
            <Link to="/">
              <NavHeader
                text="Botler"
                icon={
                  <img alt="nucleus" src="/img/botler.svg" />
                }
              />
            </Link>
          }
          globalPrimaryIcon={
            <AtlassianIcon
              label="Atlassian"
              size="medium"
            />
          }
        >
          {
            myLinks.map(link => {
              const [url, title, Icon] = link;
              return (
                <Link key={url} to={url}>
                  <AkContainerItem
                    icon={<Icon label={title} />}
                    text={title}
                    isSelected={this.context.router.isActive(url, true)}
                  />
                </Link>
              );
            })
          }
        </Nav>

        <div style={{ margin: `${4 * gridSizeInt}px ${8 * gridSizeInt}px` }}>
          {this.props.children}

          <footer style={{padding: '40px 0'}} >
            <a href="https://marketplace.atlassian.com/plugins/botler/cloud/overview">Marketplace Listing</a> - <a href="https://bitbucket.org/khanhfucius/hipchat-botler">Source Code</a>
          </footer>
        </div>

      </div>
    );
  }
}
