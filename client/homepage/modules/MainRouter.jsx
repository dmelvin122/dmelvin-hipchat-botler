import React, { PureComponent } from 'react';
import { Router, Route, browserHistory } from 'react-router';
import App from './App.jsx';
import HomePage from '../pages/HomePage.jsx';
import Library from '../pages/Library.jsx';
import Support from '../pages/Support.jsx';

export default class MainRouter extends PureComponent {
  render() {
    return (
      <Router history={browserHistory}>
        <Route component={App}>
          <Route path="/" component={HomePage} />
          <Route path="/support" component={Support} />
          <Route path="/library" component={Library} />
        </Route>
      </Router>
    );
  }
}
