import React, { PureComponent } from 'react';

export default class ServiceRow extends PureComponent {
    render() {
        return (
            <tr>
                <td>{this.props.name}</td>
                <td>{this.props.event}</td>
                <td>{this.props.description}</td>
                <td>{this.props.demo}</td>
                <td>{this.props.json}</td>
            </tr>
        )
    }
}