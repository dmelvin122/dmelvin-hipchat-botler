module.exports = {
    "development": {
        "username": "khanguyen",
        "password": null,
        "database": "room_service",
        "host": "localhost",
        "dialect": "postgres"
    },
    "test": {
        "username": process.env.PG_DATABASE_ROLE,
        "password": null,
        "database": process.env.PG_DATABASE_SCHEMA,
        "host": process.env.PG_DATABASE_HOST,
        "dialect": "postgres"
    },
    "production": {
        "url": process.env.PG_DATABASE_URL,
        "dialect": "postgres"
    }
}
