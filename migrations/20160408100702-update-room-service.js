'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    queryInterface.changeColumn(
        'RoomServices',
        'reply',
        {
          type: Sequelize.TEXT,
          allowNull: false
        }
    );

    queryInterface.addColumn(
        'RoomServices',
        'notify',
        {
          type: Sequelize.BOOLEAN,
          defaultValue: false
        }
    );
  },

  down: function (queryInterface, Sequelize) {
    queryInterface.changeColumn(
        'RoomServices',
        'reply',
        {
          type: Sequelize.STRING,
          allowNull: false
        }
    );

    queryInterface.removeColumn('RoomServices', 'notify')
  }
};
