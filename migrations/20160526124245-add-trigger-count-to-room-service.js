'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.addColumn(
        'RoomServices',
        'triggerCount',
        {
          type: Sequelize.INTEGER,
          defaultValue: 0
        }
    );
  },

  down: function (queryInterface, Sequelize) {
    queryInterface.removeColumn('RoomServices', 'triggerCount')
  }
};
