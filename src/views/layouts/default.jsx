import React from 'react';

export default class DefaultLayout extends React.Component {
  render() {
    return (
      <html>
        <head>
            <title>{this.props.title}</title>
            { /* AUI Resources */ }
            <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/6.0.1/css/aui.css" media="all"/>
            <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
            <script src="//aui-cdn.atlassian.com/aui-adg/6.0.1/js/aui.min.js"></script>
            <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/6.0.1/css/aui-experimental.min.css" media="all"/>
            <script src="//aui-cdn.atlassian.com/aui-adg/6.0.1/js/aui-experimental.min.js"></script>
            
            { /* Botler Styling */ }
            <link rel="stylesheet" href="/css/addon.css" type="text/css"/>
            
            { /* The marvelous all.js in HipChat flair. */ }
            <script src="https://www.hipchat.com/atlassian-connect/all.js"></script>
        </head>
        <body className={this.props.hipChatTheme}>
            <section id="content" role="main" className="ac-content">
                {this.props.children}
            </section>
        </body>
      </html>
    );
  }
}