import React from 'react';
import DefaultLayout from './layouts/default';

export default class Client extends React.Component {
    render() {
        return (
            <DefaultLayout title="HipChat Botler">
                <div id="root"> </div>
                <script dangerouslySetInnerHTML={{__html: `
                    window.__PRELOADED_STATE__ = ${JSON.stringify(this.props)}
                `}} />
                <script src="/js/homepage.js"></script>
            </DefaultLayout>
        )
    }
}