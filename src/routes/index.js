import cors from 'cors';
import uuid from 'uuid';
import url from 'url';
import _ from 'lodash';
import hipchat from '../lib/hipchat';
import botlerRoutes from './botler';

module.exports = (app, addon) => {
  addon.hipchat = hipchat(addon);

  // load botler routes
  botlerRoutes(app, addon);

  // simple healthcheck
  app.get('/healthcheck', (req, res) => {
    res.send('OK');
  });

  // Root route. This route will serve the `addon.json` unless a homepage URL is
  // specified in `addon.json`.
  app.get('/', (req, res) => {
      // Use content-type negotiation to choose the best way to respond
      res.format({
        // If the request content-type is text-html, it will decide which to serve up
        'text/html': () => {
          var homepage = url.parse(addon.descriptor.links.homepage);
          if (homepage.hostname === req.hostname && homepage.path === req.path) {
            res.render('homepage', addon.descriptor);
          } else {
            res.redirect(addon.descriptor.links.homepage);
          }
        },
        // This logic is here to make sure that the `addon.json` is always
        // served up when requested by the host
        'application/json': () => {
          res.redirect('/atlassian-connect.json');
        }
      });
    });
  
  app.get('/support', (req, res) => {
    res.render('homepage', addon.descriptor);
  });

  app.get('/library', (req, res) => {
    res.render('homepage', addon.descriptor);
  });

  app.get('/glance', cors(), addon.authenticate(), (req, res) => {
      res.json({
        "label": {
          "type": "html",
          "value": "<b>BOTLER</b>"
        },
        "status": {
          "type": "lozenge",
          "value": {
            "label": "BETA",
            "type": "success"
          }
        }
      });
  });

  // Clean up clients when uninstalled
  addon.on('uninstalled', (id) => {
    addon.logger.info('Deleting client details of: ', id);
    addon.settings.del(id);
  });
};
