import _ from 'lodash';
import ejs from 'ejs';
import EVENTS from '../botler/events';
import models from '../models';
import botlerDao from '../botler/dao';
import botlerHelpers from '../botler/helpers';
import BotlerRenderer from '../botler/renderer';
import replaceAll from 'replaceall';
import striptags from 'striptags';

export default function(app, addon) {
    addon.db = models(addon);
    const BotlerDAO = botlerDao(addon);
    const BotlerHelpers = botlerHelpers(addon);

    // INSTALLATION CALLBACK
    addon.on('installed', (clientKey, clientInfo, req) => {
        const roomId = req.body.roomId;
        addon.hipchat.sendMessage(clientInfo, req.body.roomId, 'The ' + addon.descriptor.name + ' add-on has been installed in this room');

        // Register webhooks of existing webhooks.
        BotlerDAO.getServicesByClientKey(clientKey).then(roomServices => {
            roomServices.forEach(service => {
                addon.logger.info('Re-registering webhook for:', service.id);
                BotlerHelpers.reRegisterWebhook(clientInfo, roomId, service).catch(err => {
                    addon.logger.error('Failed to re-register new webhook.', err);
                });
            })
        });
    });

    // SERVICE CREATE/EDIT DIALOG 
    app.get('/dialog', addon.authenticate(), (req, res) => {    
        BotlerRenderer(req, res, {
            events: EVENTS,
            hipChatTheme: 'light'
        });
    });

    // BOTLER SIDEBAR - Listing of services
    app.get('/sidebar', addon.authenticate(), (req, res) => {
        BotlerHelpers.getServices(req.clientInfo.clientKey, (parsedRoomServices) => {
            BotlerRenderer(req, res, {
                roomServices: parsedRoomServices
            });
        });
    });

    // BOTLER WEBHOOK - where the magic happens.
    app.post('/botler/callback', addon.authenticate(), (req, res) => {      
        const serviceId = req.query.id;
        const webhookId = req.body.webhook_id;

        BotlerDAO.getServiceById(serviceId).then((service) => {
            if (service) {                
                // Return a response here.
                // HipChat does not care about the response of the webhook.
                res.sendStatus(200);

                // Make sure that the webhook that triggered this service is still valid.
                if (service.webhook !== webhookId) {
                    addon.logger.warn('Triggered does not match service webhook. Deleting webhook:', webhookId);
                    BotlerHelpers.removeOldWebhook(req.body.oauth_client_id, webhookId);
                    return;
                }

                if (service.enable) {
                    let reply = service.reply;
                    Object.keys(EVENTS[service.event].attributes).forEach((attributeKey) => {
                        let option = `{{${attributeKey}}}`;
                        if (reply.indexOf(option) > -1) {
                            reply = replaceAll(option, EVENTS[service.event].attributes[attributeKey].getter(req.body), reply);
                        }
                    });

                    // match terms only if it's room_message event!
                    if (service.event === 'room_message') {

                        let message = req.body.item.message.message;

                        // check if it's an attachment
                        if (req.body.item.message.authenticated_file) {
                            message = striptags(message).match(new RegExp('(.*):.*\\n\\n.*'))[1];
                        }

                        // handle matched terms
                        let matchedTermIndex = BotlerHelpers.checkForMatchTerm(reply);
                        let matchedTerms = message.match(new RegExp(service.pattern));

                        try {
                            while (matchedTermIndex) {
                                // Prevent it from entering an infinite loop.
                                if (BotlerHelpers.checkForMatchTerm(matchedTerms[matchedTermIndex])) {
                                    break;
                                }

                                // Do the token swap.
                                reply = replaceAll('{{$' + matchedTermIndex + '}}', typeof matchedTerms[matchedTermIndex] === 'undefined' ? '' : matchedTerms[matchedTermIndex], reply);

                                matchedTermIndex = BotlerHelpers.checkForMatchTerm(reply);
                            }
                        } catch (e) {
                            addon.logger.error('Matched term parsing killed:', e);
                        }
                    }

                    // handle random terms - this should be moved to the top if
                    // substituted reply attributes and matched indices should be kept as is
                    reply = BotlerHelpers.parseRand(reply);

                    // send the damn message
                    addon.hipchat.sendMessage(
                        req.clientInfo, 
                        service.roomId, 
                        reply, 
                        { options: 
                            { format: service.replyHtml ? 'html' : 'text', 
                            notify: service.notify, 
                            color: 'gray' } 
                        }
                    );

                    // Increment triggerCount
                    service.triggerCount++;
                    service.save();
                } else {
                    addon.logger.info('Service is disabled. Not running.');
                }
            } else {
                addon.logger.warn("Could not find webhook callback for webhook with id: ", webhookId);
                BotlerHelpers.removeOldWebhook(req.body.oauth_client_id, webhookId);
                res.sendStatus(418);
            }
        });

    });

    // Used AJAX call from client to render the Edit Service Dialog.
    app.get('/botler/room_service', addon.authenticate(), (req, res) => {
        BotlerDAO.getServiceById(req.query.id).then((service) => {
            if (service) {
                let plainService = service.get({ plain: true });
                delete plainService['clientKey'];
                delete plainService['roomId'];
                res.json(plainService);
            } else {
                res.sendStatus(500);
            }
        });
    });

    // Create new service from create service dialog. 
    app.post('/botler/room_service', addon.authenticate(), (req, res) => {
        let newService = req.body,
            clientKey = req.clientInfo.clientKey,
            roomId = req.identity.roomId;

        if (!BotlerHelpers.validateService(newService)) {
            res.sendStatus(500);
            return;
        }

        // add clientKey and roomId to the service object
        _.extend(newService, {
            clientKey: clientKey,
            roomId: roomId
        });

        // parse the owners array        
        newService.owners = JSON.parse(newService.owners);

        // Create record in database.
        addon.db.RoomService.create(newService).then((service) => {
            if (service) {
                // Register the webhook on HipChat.
                addon.hipchat.addRoomWebhook(clientKey, roomId, {
                    url: addon.descriptor.links.homepage + '/botler/callback?id=' + service.id,
                    pattern: service.pattern,
                    event: service.event,
                    authentication: "jwt",
                    name: service.name
                }).then((webhook) => {
                    if (webhook.body.error) {
                        res.sendStatus(500);
                        return;
                    }

                    addon.logger.info('Webhook successfully created: ', webhook.body);
                    service.update({ webhook: webhook.body.id }); 

                    BotlerHelpers.getServices(clientKey, (parsedRoomServices) => {
                        res.json({
                            roomServices: parsedRoomServices
                        });
                    });
                });
            }
            else {
                res.sendStatus(500);
            }
        });
    });

    // Update existing service and update webhook if needed.
    app.put('/botler/room_service', addon.authenticate(), (req, res) => {
        let reqService = req.body,
            clientKey = req.clientInfo.clientKey;

        if (!BotlerHelpers.validateService(reqService)) {
            res.sendStatus(500);
            return;
        }

        BotlerDAO.getServiceById(reqService.id).then((service) => {
            if (service) {
                addon.logger.info('Updating room service with id: ', reqService.id)

                // Remove id from the object since we don't need to update it.
                // Maybe we should consider doing some more sanitisation here.
                delete reqService['id'];

                // If event is no longer room_message then clear the pattern.
                if (reqService.event !== 'room_message') {
                    reqService.pattern = '';
                }

                let oldEvent = service.event
                let oldPattern = service.pattern;

                // Define the new list of owners.
                reqService.owners = service.owners;
                let newOwner = JSON.parse(reqService.newOwner);                
                delete reqService.newOwner;

                if (!service.owners.hasOwnProperty(newOwner.id)) {
                    reqService.owners[newOwner.id] = { 
                        name: newOwner.name, 
                        mentionName: newOwner.mentionName 
                    };
                }

                // Update the service record in the database. 
                service.update(reqService).then((data) => {
                    if (BotlerHelpers.checkIfWebhookChanged(reqService, oldEvent, oldPattern)) {

                        // Log log log log some info.
                        addon.logger.info('Webhook event is different. Replacing existing webhook...');
                        addon.logger.info("Deleting webhook: ", service.webhook);

                        // Delete the webhook registered HipChat.
                        BotlerHelpers.unregisterWebhook(req.clientInfo, service.roomId, service.webhook).then((d) => {
                            addon.logger.info('Webhook successfully deleted.');
                            addon.hipchat.addRoomWebhook(req.clientInfo, service.roomId, {
                                url: addon.descriptor.links.homepage + '/botler/callback?id=' + service.id,
                                pattern: reqService.pattern,
                                event: reqService.event,
                                authentication: "jwt",
                                name: reqService.name
                            }).then((webhook) => {
                                // Create the new webhook on HipChat.
                                addon.logger.info('Webhook successfully created: ', webhook.body);
                                service.update({ webhook: webhook.body.id });
                                BotlerHelpers.getServices(clientKey, (parsedRoomServices) => {
                                    res.json({
                                        roomServices: parsedRoomServices
                                    });
                                });
                            }).catch((err) => {
                                addon.logger.error('Failed to create new webhook.', err);
                                res.sendStatus(500);
                            });
                        }).catch((err) => {
                            addon.logger.error('Failed to delete webhook.', err);
                            res.sendStatus(500);
                        });
                    } else {
                        // Carry on. Nothing to see here.
                        BotlerHelpers.getServices(clientKey, (parsedRoomServices) => {
                            res.json({
                                roomServices: parsedRoomServices
                            });
                        });
                    }
                });
            } else {
                // I'm a teapot.
                res.sendStatus(418);
            }
        });
    });

    // Delete service and remove the webhook from HipChat.
    app.delete('/botler/room_service', addon.authenticate(), (req, res) => {
        let clientKey = req.clientInfo.clientKey;
        BotlerDAO.getServiceById(req.body.id).then((service) => {
            if (service) {
                addon.logger.info("Deleting webhook: ", service.webhook);

                // Remove the webhook from HipChat.
                BotlerHelpers.unregisterWebhook(req.clientInfo, service.roomId, service.webhook).then((data) => {
                    service.destroy().then(() => {
                        BotlerHelpers.getServices(clientKey, (parsedRoomServices) => {
                            res.json({
                                roomServices: parsedRoomServices
                            });
                        });
                    });
                }).catch((err) => {
                    res.sendStatus(500);
                });
            } else {
                res.sendStatus(418);
            }
        });
    });
}
