export default function renderer(req, res, payload) {
    if (req.identity) {
        payload.identity = req.identity;
    }

    if (!payload.hipChatTheme && req.query && req.query.theme) {
        payload.hipChatTheme = req.query.theme; 
    }

    res.render('index', payload);
}