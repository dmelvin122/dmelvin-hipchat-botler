import _ from  'lodash';
import parseRand from './parse-random';
import botlerDao from './dao';

export default function (addon) {
    const BotlerDAO = botlerDao(addon);
    return {
        removeOldWebhook: function (clientKey, webhookId) {
            BotlerDAO.getAddonSettingByClientKey(clientKey)
                .then(function (result) {
                    if (result) {
                        var clientInfo = JSON.parse(result.val);
                        addon.hipchat.removeRoomWebhook(clientInfo, clientInfo.roomId, webhookId)
                            .then(function (data) {
                                addon.logger.info('Deleted old webhook: ', webhookId);
                            }).catch(function (err) {
                                addon.logger.error('Failed to delete old webhook.', err);
                            });
                    }
                });
        },
        checkForMatchTerm: function (currentReply) {
            if (typeof currentReply === 'undefined') {
                return null;
            }

            var indexOfSymbol = currentReply.indexOf('{{$');
            if (indexOfSymbol > -1) {
                var indexMatcher = currentReply.match(new RegExp('\{\{\\$(\\d+)\}\}'));
                if (indexMatcher) {
                    var matchTermNum = parseInt(indexMatcher[1]);
                    return matchTermNum;
                } else {
                    return null;
                }
            }
        },
        checkIfWebhookChanged: function(service, oldEvent, oldPattern) {
            return  service.event !== oldEvent || ( 
                        service.event === oldEvent && 
                        oldEvent === 'room_message' && 
                        service.pattern !== oldPattern
                    );
        },
        getServices: function(clientKey, callback) {
            BotlerDAO.getServicesByClientKey(clientKey)
            .then((roomServices) => {
                let view, data;
                const parsedRoomServices = _.map(roomServices, (roomService) => {
                    let dataValues = roomService.dataValues;
                    dataValues.encodedName = roomService.name.replace(/\/|%/g, '-');
                    dataValues.ownersQuery = Object.keys(roomService.owners) // THIS IS SHIT HACK. BUT HOW TO SEARCH?
                                                .map(key => `${roomService.owners[key].mentionName} ${roomService.owners[key].name}`).join(' ');
                    return dataValues;
                });

                callback(parsedRoomServices);
            });
        },
        validateService: function(service) {
            let valid = true;
            valid = valid && !!service.name;
            if (service.event === 'room_message') {
                valid = valid && !!service.pattern;
            }
            return valid;
        },
        registerWebhook: function(clientInfo, roomId, service) {
            const payload = {
                url: `${addon.descriptor.links.homepage}/botler/callback?id=${service.id}`,
                pattern: service.pattern,
                event: service.event,
                authentication: 'jwt',
                name: service.name
            };
            return addon.hipchat.addRoomWebhook(clientInfo, roomId, payload);
        },
        unregisterWebhook: function(clientInfo, roomId, webhook) {
            return addon.hipchat.removeRoomWebhook(clientInfo, roomId, webhook);
        },
        reRegisterWebhook: function(clientInfo, roomId, service) {
            if(service.webhook) {
                return this.unregisterWebhook(clientInfo, roomId, service.webhook)
                    .then((d) => {
                        this.registerWebhook(clientInfo, roomId, service).then(webhook => {
                            addon.logger.info('Webhook successfully created: ', webhook.body);
                            service.update({ webhook: webhook.body.id });
                        }).catch(err => {
                            addon.logger.error('Failed to create new webhook.', err);
                        });
                    });
            }

            return this.registerWebhook(clientInfo, roomId, service);
        },
        parseRand: parseRand
    }
}