import _ from 'lodash';

// Event definitions used for Botler
const COMMON_EVENT_ATTRIBUTES = {
    roomName: {
        friendlyName: "Room Name",
        description: "Name of the room.",
        getter: function (payload) {
            return payload.item.room.name;
        }
    }
};

export default {
    room_message: {
        friendlyName: 'A new message in the room',
        attributes: _.extend({
            senderName: {
                friendlyName: "Sender Name",
                description: "The display user name.",
                getter: function (payload) {
                    return payload.item.message.from.name;
                }
            },
            senderMentionName: {
                friendlyName: "Sender Mention Name",
                description: "User's @mention name.",
                getter: function (payload) {
                    return '@' + payload.item.message.from.mention_name;
                }
            },
            message: {
                friendlyName: "Message",
                description: 'The message sent',
                getter: function (payload) { return payload.item.message.message }
            },
            mentions: {
                friendlyName: "Mentions",
                description: 'A list of mentioned users in this message.',
                getter: function (payload) {
                    return _.map(payload.item.message.mentions, function (mention) {
                        return '@' + mention['mention_name'];
                    }).join(' ')
                }
            }
        }, COMMON_EVENT_ATTRIBUTES)
    },
    room_enter: {
        friendlyName: 'A user enters the room',
        attributes: _.extend({
            senderName: {
                friendlyName: "Sender Name",
                description: "The display user name.",
                getter: function (payload) {
                    return payload.item.sender.name;
                }
            },
            senderMentionName: {
                friendlyName: "Sender Mention Name",
                description: "User's @mention name.",
                getter: function (payload) {
                    return '@' + payload.item.sender.mention_name;
                }
            }
        }, COMMON_EVENT_ATTRIBUTES)
    },
    room_exit: {
        friendlyName: 'A user exits the room',
        attributes: _.extend({
            senderName: {
                friendlyName: "Sender Name",
                description: "The display user name.",
                getter: function (payload) {
                    return payload.item.sender.name;
                }
            },
            senderMentionName: {
                friendlyName: "Sender Mention Name",
                description: "User's @mention name.",
                getter: function (payload) {
                    return '@' + payload.item.sender.mention_name;
                }
            }
        }, COMMON_EVENT_ATTRIBUTES)
    },
    room_file_upload: {
        friendlyName: 'A user uploads a file to the room',
        attributes: _.extend({
            fileName: {
                friendlyName: "File Name",
                description: 'The name of the uploaded file',
                getter: function (payload) { return payload.item.file.name }
            },
            fileSize: {
                friendlyName: "File Size",
                description: 'The size of the file in bytes',
                getter: function (payload) { return payload.item.file.size }
            },
            fileUrl: {
                friendlyName: "File URL",
                description: 'The URL of the file',
                getter: function (payload) { return payload.item.file.url }
            },
            fileThumbnail: {
                friendlyName: "File Thumbnail",
                description: 'The URL of the thumbnail if it exists',
                getter: function (payload) { return payload.item.file.thumb_url }
            }
        }, COMMON_EVENT_ATTRIBUTES)
    },
    room_topic_change: {
        friendlyName: 'Someone changes the room topic',
        attributes: _.extend({
            topic: {
                friendlyName: "Topic",
                description: 'The new topic.',
                getter: function (payload) { return payload.item.topic }
            }
        }, COMMON_EVENT_ATTRIBUTES)
    }
};