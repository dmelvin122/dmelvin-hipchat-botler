// Hello fellow Daoist

export default function (addon) {
    return {
        getAddonSettingByClientKey: function (clientKey) {
            return addon.db.AddonSetting.findOne({ 
                where: { 
                    clientKey: clientKey,
                    key: 'clientInfo' 
                } 
            });
        },
        getServicesByClientKey: function (clientKey) {
            return addon.db.RoomService.findAll({ 
                where: { 
                    clientKey: clientKey }, 
                    order: 'id DESC' 
                });
        },
        getServiceById: function (serviceId) {
            return addon.db.RoomService.findOne({ 
                where: { 
                    id: serviceId 
                } 
            });
        },
    }
}