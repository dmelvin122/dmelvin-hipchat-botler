var webpack = require('webpack');
var path = require('path');

var APP_DIR = path.resolve(__dirname, 'client');
var BUILD_DIR = path.resolve(__dirname, 'public/js');

var config = {
  entry: {
    bundle: APP_DIR + '/index.jsx',
    homepage: APP_DIR + '/homepage/index.jsx',
  },
  output: {
    path: BUILD_DIR,
    filename: '[name].js'
  },
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : APP_DIR,
        loader : 'babel'
      },
      { test: /\.css$/, 
        loader: "style-loader!css-loader" 
      }
    ]
  }
};

module.exports = config;